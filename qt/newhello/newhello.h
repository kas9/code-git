#ifndef NEWHELLO_H
#define NEWHELLO_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class newhello; }
QT_END_NAMESPACE

class newhello : public QMainWindow
{
    Q_OBJECT

public:
    newhello(QWidget *parent = nullptr);
    ~newhello();

private:
    Ui::newhello *ui;
};
#endif // NEWHELLO_H
