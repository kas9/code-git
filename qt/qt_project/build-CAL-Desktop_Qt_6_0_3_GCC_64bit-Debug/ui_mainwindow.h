/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 6.0.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralwidget;
    QGridLayout *gridLayout;
    QPushButton *Button3;
    QPushButton *minus;
    QPushButton *memclear;
    QPushButton *devide;
    QPushButton *Button4;
    QPushButton *pushButton_12;
    QPushButton *clear;
    QPushButton *Button6;
    QPushButton *Button1;
    QPushButton *equal;
    QLineEdit *DISPLAY;
    QPushButton *Button9;
    QPushButton *plus;
    QPushButton *changesing;
    QPushButton *Button7;
    QPushButton *memadd;
    QPushButton *multiplay;
    QPushButton *Button5;
    QPushButton *Button8;
    QPushButton *Button2;
    QPushButton *Button0;
    QMenuBar *menubar;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(649, 268);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        gridLayout = new QGridLayout(centralwidget);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        Button3 = new QPushButton(centralwidget);
        Button3->setObjectName(QString::fromUtf8("Button3"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(Button3->sizePolicy().hasHeightForWidth());
        Button3->setSizePolicy(sizePolicy1);
        Button3->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button3, 6, 2, 1, 1);

        minus = new QPushButton(centralwidget);
        minus->setObjectName(QString::fromUtf8("minus"));
        sizePolicy1.setHeightForWidth(minus->sizePolicy().hasHeightForWidth());
        minus->setSizePolicy(sizePolicy1);
        minus->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(minus, 7, 3, 1, 1);

        memclear = new QPushButton(centralwidget);
        memclear->setObjectName(QString::fromUtf8("memclear"));
        sizePolicy1.setHeightForWidth(memclear->sizePolicy().hasHeightForWidth());
        memclear->setSizePolicy(sizePolicy1);
        memclear->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(memclear, 5, 4, 1, 1);

        devide = new QPushButton(centralwidget);
        devide->setObjectName(QString::fromUtf8("devide"));
        sizePolicy1.setHeightForWidth(devide->sizePolicy().hasHeightForWidth());
        devide->setSizePolicy(sizePolicy1);
        devide->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(devide, 3, 3, 1, 1);

        Button4 = new QPushButton(centralwidget);
        Button4->setObjectName(QString::fromUtf8("Button4"));
        sizePolicy1.setHeightForWidth(Button4->sizePolicy().hasHeightForWidth());
        Button4->setSizePolicy(sizePolicy1);
        Button4->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button4, 5, 0, 1, 1);

        pushButton_12 = new QPushButton(centralwidget);
        pushButton_12->setObjectName(QString::fromUtf8("pushButton_12"));
        sizePolicy1.setHeightForWidth(pushButton_12->sizePolicy().hasHeightForWidth());
        pushButton_12->setSizePolicy(sizePolicy1);
        pushButton_12->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(pushButton_12, 6, 4, 1, 1);

        clear = new QPushButton(centralwidget);
        clear->setObjectName(QString::fromUtf8("clear"));
        sizePolicy1.setHeightForWidth(clear->sizePolicy().hasHeightForWidth());
        clear->setSizePolicy(sizePolicy1);
        clear->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(clear, 7, 0, 1, 1);

        Button6 = new QPushButton(centralwidget);
        Button6->setObjectName(QString::fromUtf8("Button6"));
        sizePolicy1.setHeightForWidth(Button6->sizePolicy().hasHeightForWidth());
        Button6->setSizePolicy(sizePolicy1);
        Button6->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button6, 5, 2, 1, 1);

        Button1 = new QPushButton(centralwidget);
        Button1->setObjectName(QString::fromUtf8("Button1"));
        sizePolicy1.setHeightForWidth(Button1->sizePolicy().hasHeightForWidth());
        Button1->setSizePolicy(sizePolicy1);
        Button1->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button1, 6, 0, 1, 1);

        equal = new QPushButton(centralwidget);
        equal->setObjectName(QString::fromUtf8("equal"));
        sizePolicy1.setHeightForWidth(equal->sizePolicy().hasHeightForWidth());
        equal->setSizePolicy(sizePolicy1);
        equal->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(equal, 7, 4, 1, 1);

        DISPLAY = new QLineEdit(centralwidget);
        DISPLAY->setObjectName(QString::fromUtf8("DISPLAY"));
        sizePolicy.setHeightForWidth(DISPLAY->sizePolicy().hasHeightForWidth());
        DISPLAY->setSizePolicy(sizePolicy);
        QFont font;
        font.setFamily(QString::fromUtf8("RocknRoll One"));
        font.setPointSize(11);
        DISPLAY->setFont(font);
        DISPLAY->setStyleSheet(QString::fromUtf8("QLineEdit{\n"
"	background-color: gray;\n"
"	border: 1px solid gray;\n"
"	color: red\n"
"}\n"
""));

        gridLayout->addWidget(DISPLAY, 0, 0, 1, 5);

        Button9 = new QPushButton(centralwidget);
        Button9->setObjectName(QString::fromUtf8("Button9"));
        sizePolicy1.setHeightForWidth(Button9->sizePolicy().hasHeightForWidth());
        Button9->setSizePolicy(sizePolicy1);
        Button9->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button9, 3, 2, 1, 1);

        plus = new QPushButton(centralwidget);
        plus->setObjectName(QString::fromUtf8("plus"));
        sizePolicy1.setHeightForWidth(plus->sizePolicy().hasHeightForWidth());
        plus->setSizePolicy(sizePolicy1);
        plus->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(plus, 6, 3, 1, 1);

        changesing = new QPushButton(centralwidget);
        changesing->setObjectName(QString::fromUtf8("changesing"));
        sizePolicy1.setHeightForWidth(changesing->sizePolicy().hasHeightForWidth());
        changesing->setSizePolicy(sizePolicy1);
        changesing->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(changesing, 7, 2, 1, 1);

        Button7 = new QPushButton(centralwidget);
        Button7->setObjectName(QString::fromUtf8("Button7"));
        sizePolicy1.setHeightForWidth(Button7->sizePolicy().hasHeightForWidth());
        Button7->setSizePolicy(sizePolicy1);
        Button7->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button7, 3, 0, 1, 1);

        memadd = new QPushButton(centralwidget);
        memadd->setObjectName(QString::fromUtf8("memadd"));
        sizePolicy1.setHeightForWidth(memadd->sizePolicy().hasHeightForWidth());
        memadd->setSizePolicy(sizePolicy1);
        memadd->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(memadd, 3, 4, 1, 1);

        multiplay = new QPushButton(centralwidget);
        multiplay->setObjectName(QString::fromUtf8("multiplay"));
        sizePolicy1.setHeightForWidth(multiplay->sizePolicy().hasHeightForWidth());
        multiplay->setSizePolicy(sizePolicy1);
        multiplay->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(multiplay, 5, 3, 1, 1);

        Button5 = new QPushButton(centralwidget);
        Button5->setObjectName(QString::fromUtf8("Button5"));
        sizePolicy1.setHeightForWidth(Button5->sizePolicy().hasHeightForWidth());
        Button5->setSizePolicy(sizePolicy1);
        Button5->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button5, 5, 1, 1, 1);

        Button8 = new QPushButton(centralwidget);
        Button8->setObjectName(QString::fromUtf8("Button8"));
        sizePolicy1.setHeightForWidth(Button8->sizePolicy().hasHeightForWidth());
        Button8->setSizePolicy(sizePolicy1);
        Button8->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button8, 3, 1, 1, 1);

        Button2 = new QPushButton(centralwidget);
        Button2->setObjectName(QString::fromUtf8("Button2"));
        sizePolicy1.setHeightForWidth(Button2->sizePolicy().hasHeightForWidth());
        Button2->setSizePolicy(sizePolicy1);
        Button2->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #50fa7b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: red;\n"
"}"));

        gridLayout->addWidget(Button2, 6, 1, 1, 1);

        Button0 = new QPushButton(centralwidget);
        Button0->setObjectName(QString::fromUtf8("Button0"));
        sizePolicy1.setHeightForWidth(Button0->sizePolicy().hasHeightForWidth());
        Button0->setSizePolicy(sizePolicy1);
        Button0->setStyleSheet(QString::fromUtf8("QPushButton{\n"
"	background-color: #FFBC00;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: black;\n"
"}\n"
"\n"
"QPushButton:pressed{\n"
"	background-color: 543e5b;\n"
"	border: 1px solid gray;\n"
"	padding: 5px;\n"
"	color: green;\n"
"}"));

        gridLayout->addWidget(Button0, 7, 1, 1, 1);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 649, 20));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QCoreApplication::translate("MainWindow", "MainWindow", nullptr));
        Button3->setText(QCoreApplication::translate("MainWindow", "3", nullptr));
        minus->setText(QCoreApplication::translate("MainWindow", "-", nullptr));
        memclear->setText(QCoreApplication::translate("MainWindow", "m-", nullptr));
        devide->setText(QCoreApplication::translate("MainWindow", "/", nullptr));
        Button4->setText(QCoreApplication::translate("MainWindow", "4", nullptr));
        pushButton_12->setText(QCoreApplication::translate("MainWindow", "M", nullptr));
        clear->setText(QCoreApplication::translate("MainWindow", "AC", nullptr));
        Button6->setText(QCoreApplication::translate("MainWindow", "6", nullptr));
        Button1->setText(QCoreApplication::translate("MainWindow", "1", nullptr));
        equal->setText(QCoreApplication::translate("MainWindow", "=", nullptr));
        DISPLAY->setText(QCoreApplication::translate("MainWindow", "0.0", nullptr));
        Button9->setText(QCoreApplication::translate("MainWindow", "9", nullptr));
        plus->setText(QCoreApplication::translate("MainWindow", "+", nullptr));
        changesing->setText(QCoreApplication::translate("MainWindow", "+/-", nullptr));
        Button7->setText(QCoreApplication::translate("MainWindow", "7", nullptr));
        memadd->setText(QCoreApplication::translate("MainWindow", "m+", nullptr));
        multiplay->setText(QCoreApplication::translate("MainWindow", "*", nullptr));
        Button5->setText(QCoreApplication::translate("MainWindow", "5", nullptr));
        Button8->setText(QCoreApplication::translate("MainWindow", "8", nullptr));
        Button2->setText(QCoreApplication::translate("MainWindow", "2", nullptr));
        Button0->setText(QCoreApplication::translate("MainWindow", "0", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
