#ifndef LAPTOP_H
#define LAPTOP_H

#include <QObject>
#include <QDebug>

class Laptop : public QObject
{
    Q_OBJECT
public:
    //constructed
    explicit Laptop(QObject *parent = nullptr, QString name  = "");
    //deconstruction
    ~Laptop();

    int weight;
    QString name;
    double asKilo();
    void test();
signals:

};

#endif // LAPTOP_H
