#include "laptop.h"

Laptop::Laptop(QObject *parent, QString name) : QObject(parent)
{
    this -> name = name;
    qInfo() << this << "constractor" << name;
}

Laptop::~Laptop()
{
    qInfo() << this << "deconstractor" << name;
}

double Laptop::asKilo()
{
    return weight * 0.453592;
}

void Laptop::test()
{
    qInfo() << this << "Test: " << name << "kilo: " << this->asKilo();
}
