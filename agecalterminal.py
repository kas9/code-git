import random
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
import calendar

def calculate_age(dob, age_date, dob_time, age_time, lucky_output_digits):
    dob = datetime.strptime(dob, "%Y-%m-%d %H:%M:%S")
    age_date = datetime.strptime(age_date, "%Y-%m-%d %H:%M:%S")

    delta = relativedelta(age_date, dob)
    age_difference = age_date - dob
    leap_years = calendar.leapdays(dob.year, age_date.year)

    days = age_difference.days - leap_years
    years, days = divmod(days, 365)
    months, days = divmod(days, 30)

    input_number_str = str(age_difference.days * 24 * 3600 + delta.hours * 3600 + delta.minutes * 60 + delta.seconds)
    digit_sum = sum(int(char) for char in input_number_str)

    def generate_lucky_number(input_str, lucky_output_digits):

        digit_sum = sum(int(char) for char in input_number_str)
        #for char in input_str: ## same thing
            #digit_sum += int(char)
        x = digit_sum

        if len(str(digit_sum)) >= lucky_output_digits:
            while len(str(digit_sum)) != lucky_output_digits:
                digit_sum_str = str(digit_sum)
                digit_sum = sum(int(char) for char in digit_sum_str)
            result = digit_sum

        else:
            print("Debug : seconds_digits > lucky_output_digits\n\n")
            while len(str(digit_sum)) != lucky_output_digits:
                digit_sum_str = str(digit_sum)
                digit_sums = sum(int(char) for char in digit_sum_str)
                random_digits = ''.join(random.sample(input_number_str, 1))
                digit_sum = (random_digits) + str(digit_sum)
            result = digit_sum
        return result


    lucky_number = generate_lucky_number(input_number_str, lucky_output_digits)

    result_text = f"{delta.years} years {delta.months} months {delta.days} days\n" \
                  f"{delta.years * 12 + delta.months} months {delta.days} days\n" \
                  f"{age_difference.days // 7} weeks {age_difference.days % 7} days\n" \
                  f"{age_difference.days} days\n" \
                  f"{age_difference.days * 24 + delta.hours} hours\n" \
                  f"{(age_difference.days * 24 + delta.hours) * 60 + delta.minutes} minutes\n" \
                  f"{(age_difference.days * 24 + delta.hours) * 3600 + delta.seconds} seconds\n" \
                  f"{digit_sum} is the sum of the seconds string digits\n" \
                  f"{lucky_output_digits} is the output requirement digits\n" \
                  f"{lucky_number} is your lucky number"

    return result_text

def main():
    print("Welcome to the Age Calculator")
    dob = input("Enter your Date of Birth (YYYY-MM-DD HH:MM:SS): ")
    age_date = input("Enter the Date to Calculate Age at (YYYY-MM-DD HH:MM:SS): ")
    lucky_output_digits = int(input("Enter the number of Lucky Output Digits: "))

    result_text = calculate_age(dob, age_date, 0, 0, lucky_output_digits)

    print("Results:")
    print(result_text)

if __name__ == "__main__":
    main()
