#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
    cout.setf(ios::fixed);
    float p1,p2,p3,p4;
    cin>>p1>>p2>>p3>>p4;
    float avg = ((p1 * 2 + p2 * 3 + p3 * 4 + p4) /10);
    cout<<fixed;
    cout <<setprecision(1)<<"Media: "<< avg<<endl;
    if(avg >= 7.0) {
        cout <<"Aluno aprovado."<<endl;
    }
    else if(avg >= 5.0) {
        cout <<"Aluno em exame."<<endl;
        float nw;
        cin >> nw;
        cout << "Nota do exame: "<<nw<<endl;
        float l = ((nw+avg)/2.0);
        if(nw + avg / 2.0 > 5.0) {
            cout<<"Aluno aprovado."<<endl;
        }
        else {
            cout<<"Aluno reprovado."<<endl;
        }
        cout <<setprecision(1)<<"Media final: "<<l<<endl;
    }
    else{
        cout << "Aluno reprovado."<<endl;
    }
    return 0;
}

